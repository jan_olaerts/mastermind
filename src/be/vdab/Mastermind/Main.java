package be.vdab.Mastermind;

import java.util.Random;
import java.util.Scanner;

public class Main {

    private static Scanner scanner = new Scanner(System.in);
    private static Random random = new Random();

    public static void main(String[] args) {

        System.out.println("Welcome to Mastermind!");
        System.out.println("The answer consists of a 4-digit number");
        System.out.println("There are no duplicate digits in the answer");

        int[] answer = randomizeAnswer();

        int[] guessArray = guess();

        boolean check = check(guessArray, answer);
        System.out.println(check);

        while(!check) {
            guessArray = guess();
            check = check(guessArray, answer);
        }

        if(check) {
            System.out.println("Congratulations, you guessed the answer!");
        }
    }

    public static int[] randomizeAnswer() {
        int[] answer = new int[4];
        answer[0] = 1 + random.nextInt(9);

        for(int i = 1; i < answer.length; i++) {
            answer[i] = 1 + random.nextInt(9);

            for(int j = 0; j < i; j++) {
                if(answer[i] == answer[j]) {
                    i--;
                }
            }
        }

        for (int i = 0; i < answer.length; i++) {
            System.out.println(answer[i]);
        }

        return answer;
    }

    public static int[] guess() {
        System.out.println("Type in four digits: ");
        int[] guessInt = new int[4];

        guessInt[0] = typeNumber();
        guessInt[1] = typeNumber();
        guessInt[2] = typeNumber();
        guessInt[3] = typeNumber();

        return guessInt;
    }

    public static int typeNumber() {

        int num = 0;
        while(true) {
            try {
                num = scanner.nextInt();

                if(num > 9 || num < 0) {
                    throw new RuntimeException();
                }

                break;
            } catch(RuntimeException e) {
                System.out.println("Type in a valid digit");
            } finally {
                scanner.nextLine();
            }
        }

        return num;
    }

    public static boolean check(int[] guessArray, int[] answerArray) {

        char[] solution = new char[4];
        char[] arrangedSolution = new char[4];

        int index = 0;
        for(int i = 0; i < guessArray.length; i++) {

            if(guessArray[i] == answerArray[i]) {
                solution[i] = '+';
                arrangedSolution[index++] = '+';
            }
        }

        for(int i = 0; i < guessArray.length; i++) {

            for(int j = 0; j < answerArray.length; j++) {

                if(guessArray[i] == answerArray[j] && solution[i] != '+' && solution[j] != '+') {
                    solution[i] = '-';
                    arrangedSolution[index++] = '-';
                    answerArray[j] = 0;
                    break;
                }
            }
        }

        String stringSolution = new String(arrangedSolution);
        System.out.println(stringSolution);
        return stringSolution.equals("++++");
    }
}